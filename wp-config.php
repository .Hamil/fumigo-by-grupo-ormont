<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'grupo_ormont' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');//this is the FTP configuration

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mUS0GAkvlNz }0+(p.P.7@]!a-W`m.sm{yVXk*jncX6db7k1y#ZfslpR9@2`4BQ,' );
define( 'SECURE_AUTH_KEY',  'J9_X5(_svm5 Z![hRT14:a7o0V& J:(JQ+(BvSeb/vIz{rU|>=UGfd8=VJ-iy`rI' );
define( 'LOGGED_IN_KEY',    'dZD/NFA<gtG+oPFy/+8%)T;^?#rq|uU;f6)}FE;)Z!9hn(r2]j[*qr)S.Lf 3,d3' );
define( 'NONCE_KEY',        '+,1-B,qE 55WZ6,+6srv9p#b zo}m(1_]d?6MOLaSU,29.Z>,Ab}jMI5K$MD`o2T' );
define( 'AUTH_SALT',        '*Bs{+j^o?lOJO<L._<q1?bxTjluWsWfC.+X#<W_I.OU3A{M{+uP!6+K[fUB2]sI2' );
define( 'SECURE_AUTH_SALT', '#C}k1LdgdAVa0Djj/=>i$T>D+.1;)[_9;#<@BY/$~zUt>Nu@odl#e88-gMqh}8)H' );
define( 'LOGGED_IN_SALT',   'q/e8f%z^nZvWBN?)aPq,z6E$#|1n&`L<dM4yPnZ:SF}D,llZb. j_>@N+d|RgKy&' );
define( 'NONCE_SALT',       'rJ?r+9nu7yQzYdDB/kxK rZ}-plE:)pR.f[G#(6FV8w|Sk%Xp]-{n<,Vzdxnvl-q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
